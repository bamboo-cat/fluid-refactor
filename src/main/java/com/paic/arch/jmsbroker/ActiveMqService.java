package com.paic.arch.jmsbroker;

import javax.jms.Connection;
import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * MQ服务（ActiveMQ实现方式）
 * @author xjiafei
 */
public class ActiveMqService implements IMqService {
	@Autowired
	private IBrokerService brokerService;	//代理服务 	依赖注入
	
	
	public void setBrokerService(IBrokerService brokerService) {
		this.brokerService = brokerService;
	}

	public IBrokerService getBrokerService() {
		return brokerService;
	}

	public Connection getConnection(String brokerUrl) throws JMSException {
		//调用连接工厂创建一个连接
		 ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerUrl);
         return connectionFactory.createConnection();
	}
	public long getDestinationStatisticsFor(String aDestinationName,String brokerUrl) throws Exception{
		//统计目标队列中待处理消息数
		Broker regionBroker = getBrokerService().getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(aDestinationName)) {
                return destination.getDestinationStatistics().getMessages().getCount();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, brokerUrl));
	}

}
