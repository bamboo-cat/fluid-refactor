package com.paic.arch.jmsbroker;

import org.apache.activemq.broker.Broker;

/**
 * MQ代理服务
 * @author xjiafei
 */
public interface IBrokerService {
	
	 /**
	  * 设置是否持久化
	 * @param persistent
	 */
	void setPersistent(boolean persistent);
	 /**
	  * 根据url添加连接
	 * @param brokerUrl
	 */
	void addConnector(String brokerUrl) throws Exception;
	/**
	 * 获取区域代理
	 * @return
	 */
	Broker getRegionBroker();
	 /**
	 * 服务启动
	 */
	void start() throws Exception;
	 /**
	 * 服务停止
	 */
	void stop() throws Exception;
 }