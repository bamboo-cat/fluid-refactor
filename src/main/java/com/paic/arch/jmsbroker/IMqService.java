package com.paic.arch.jmsbroker;

import javax.jms.Connection;
import javax.jms.JMSException;

/**
 * 
 * MQ服务接口
 * @author xjiafei
 */
public interface IMqService {
	 /**
	  * 获取代理服务
	 * @return
	 */
	IBrokerService getBrokerService();
	 /**获取MQ连接
	 * @param brokerUrl
	 * @return
	 */
	Connection getConnection(String brokerUrl) throws JMSException;
	 /**
	  * 获取目标队列中待处理消息数
	 * @param aDestinationName	队列名称
	 * @return
	 */
	long getDestinationStatisticsFor(String aDestinationName,String brokerUrl) throws Exception;
 }