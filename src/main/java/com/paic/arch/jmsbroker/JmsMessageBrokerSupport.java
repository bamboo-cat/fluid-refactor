package com.paic.arch.jmsbroker;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;

import javax.jms.*;
import java.lang.IllegalStateException;
import org.springframework.beans.factory.annotation.Autowired;

import static org.slf4j.LoggerFactory.getLogger;
import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;

/**
 * 分析：代码部分：
 * 	   SocketFinder是关于socket的一个工具类，无需重构
 * 	   JmsMessageBrokerSupport具备4个功能：创建MQ服务代理、停止MQ服务代理、发送消息、读取消息、监控消息队列是否处理完毕
 *     因为是消息模块的功能，是一个基础功能，依赖的多是基础组件，因此基础组件部分需要重构
 * 	   JmsMessageBrokerSupportTest用例包括消息发送、消息接收、未读取到消息的异常
 * 	         需求部分： 1.新API要满足所有应用程序中调用
 * 			  2.根据SOLID原则，切换不同MQ代理技术的时候，要灵活且低耦合
 *            3.按照类的SRP设计
 *            4.最好设计成流式API
 *            5.需要向下兼容
 *            6.如果需要可以采用依赖注入
 * 设计：重构JmsMessageBrokerSupport中依赖具体MQ部分：创建MQ服务、停止MQ服务、发送和读取消息的公共方法、监控消息队列是否处理完毕
 * 	   设计一个MQ服务接口：具备提供MQ代理服务（设置基本属性、启动、停止）、提供MQ连接和监控消息队列   不同的MQ代理技术单独实现该服务接口  详见设计图（PD）
 * 	   向下兼容有两种实现方式：1.新建类继承原类，新的应用程序使用新建的子类
 * 					 2.原类中保留原来的实现，在这基础上增加判断调用程序版本，新版本走新逻辑 （考虑尽量无需更改测试用例，采用这种方式）
 *   流式API有了解过，但是平时用的不多
 * @author xjiafei
 */
public class JmsMessageBrokerSupport {
    private static final Logger LOG = getLogger(JmsMessageBrokerSupport.class);
    private static final int ONE_SECOND = 1000;
    private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
    public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";
    
    @Autowired
    private IMqService mqService;	//mq服务	依赖注入  
    private String brokerUrl;
    private BrokerService brokerService;

    private JmsMessageBrokerSupport(String aBrokerUrl) {
        brokerUrl = aBrokerUrl;
    }

    public static JmsMessageBrokerSupport createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
        return createARunningEmbeddedBrokerAt(DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000));
    }

    public static JmsMessageBrokerSupport createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
        LOG.debug("Creating a new broker at {}", aBrokerUrl);
        JmsMessageBrokerSupport broker = bindToBrokerAtUrl(aBrokerUrl);
        broker.createEmbeddedBroker();
        broker.startEmbeddedBroker();
        return broker;
    }
    /**
     * 是否向后兼容（旧版本mqService为空）
     * @return
     */
    private boolean isBackComp(){
    	return mqService==null;
    }
    private void createEmbeddedBroker() throws Exception {
    	//向后兼容
    	if(isBackComp()){
    		brokerService = new BrokerService();
            brokerService.setPersistent(false);
            brokerService.addConnector(brokerUrl);
    	}else{
	    	mqService.getBrokerService().setPersistent(false);
	    	mqService.getBrokerService().addConnector(brokerUrl);
    	}
    }

    public static JmsMessageBrokerSupport bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
        return new JmsMessageBrokerSupport(aBrokerUrl);
    }

    private void startEmbeddedBroker() throws Exception {
    	//向后兼容
    	if(isBackComp()){
    		brokerService.start();
    	}else{
    		mqService.getBrokerService().start();
    	}
    }

    public void stopTheRunningBroker() throws Exception {
    	//向后兼容
    	if(isBackComp()){
	        if (brokerService == null) {
	            throw new IllegalStateException("Cannot stop the broker from this API: " +
	                    "perhaps it was started independently from this utility");
	        }
	        brokerService.stop();
	        brokerService.waitUntilStopped();
    	}else{
    		mqService.getBrokerService().stop();
    	}
    }

    public final JmsMessageBrokerSupport andThen() {
        return this;
    }

    public final String getBrokerUrl() {
        return brokerUrl;
    }

    public JmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend) {
        executeCallbackAgainstRemoteBroker(brokerUrl, aDestinationName, (aSession, aDestination) -> {
            MessageProducer producer = aSession.createProducer(aDestination);
            producer.send(aSession.createTextMessage(aMessageToSend));
            producer.close();
            return "";
        });
        return this;
    }

    public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
        return retrieveASingleMessageFromTheDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
    }

    public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
        return executeCallbackAgainstRemoteBroker(brokerUrl, aDestinationName, (aSession, aDestination) -> {
            MessageConsumer consumer = aSession.createConsumer(aDestination);
            Message message = consumer.receive(aTimeout);
            if (message == null) {
                throw new NoMessageReceivedException(String.format("No messages received from the broker within the %d timeout", aTimeout));
            }
            consumer.close();
            return ((TextMessage) message).getText();
        });
    }

    private String executeCallbackAgainstRemoteBroker(String aBrokerUrl, String aDestinationName, JmsCallback aCallback) {
        Connection connection = null;
        String returnValue = "";
        try {
        	//向后兼容
        	if(isBackComp()){
        		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(aBrokerUrl);
                connection = connectionFactory.createConnection();
        	}else{
        		//调用mq服务获取连接
        		connection= mqService.getConnection(aBrokerUrl);
        	}
            connection.start();
            returnValue = executeCallbackAgainstConnection(connection, aDestinationName, aCallback);
        } catch (JMSException jmse) {
            LOG.error("failed to create connection to {}", aBrokerUrl);
            throw new IllegalStateException(jmse);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close connection to broker at []", aBrokerUrl);
                    throw new IllegalStateException(jmse);
                }
            }
        }
        return returnValue;
    }

    interface JmsCallback {
        String performJmsFunction(Session aSession, Destination aDestination) throws JMSException;
    }

    private String executeCallbackAgainstConnection(Connection aConnection, String aDestinationName, JmsCallback aCallback) {
        Session session = null;
        try {
            session = aConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue(aDestinationName);
            return aCallback.performJmsFunction(session, queue);
        } catch (JMSException jmse) {
            LOG.error("Failed to create session on connection {}", aConnection);
            throw new IllegalStateException(jmse);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close session {}", session);
                    throw new IllegalStateException(jmse);
                }
            }
        }
    }

    public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
    	//向后兼容
    	if(isBackComp()){
    		return getDestinationStatisticsFor(aDestinationName).getMessages().getCount();
    	}else{
    		//调用mq服务获取待处理消息数
    		return mqService.getDestinationStatisticsFor(aDestinationName,brokerUrl);
    	}
    }

    public boolean isEmptyQueueAt(String aDestinationName) throws Exception {
        return getEnqueuedMessageCountAt(aDestinationName) == 0;
    }
    /**
     * 旧版本使用，新版本通过调用mq服务实现
     * @param aDestinationName
     * @return
     * @throws Exception
     */
    @Deprecated
    private DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception {
        Broker regionBroker = brokerService.getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(aDestinationName)) {
                return destination.getDestinationStatistics();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, brokerUrl));
    }

    public class NoMessageReceivedException extends RuntimeException {
        public NoMessageReceivedException(String reason) {
            super(reason);
        }
    }

	public IMqService getMqService() {
		return mqService;
	}

	public void setMqService(IMqService mqService) {
		this.mqService = mqService;
	}
    
}