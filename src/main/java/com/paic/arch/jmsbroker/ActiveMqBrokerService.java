package com.paic.arch.jmsbroker;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * MQ代理服务实现（ActiveMQ实现）
 * @author xjiafei
 */
public class ActiveMqBrokerService implements IBrokerService {
	@Autowired
	private BrokerService brokerService;	//ActionMq代理服务 依赖注入
	

	public void setPersistent(boolean persistent) {
		brokerService.setPersistent(persistent);
	}

	public void addConnector(String brokerUrl) throws Exception {
		brokerService.addConnector(brokerUrl);
	}
	public Broker getRegionBroker() {
		return brokerService.getRegionBroker();
	}
	public void start()  throws Exception{
		brokerService.start();
	}
	public void stop()  throws Exception{
		//有可能已经停止 需判空
		if (brokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        brokerService.stop();
        brokerService.waitUntilStopped();
	}

	
	public BrokerService getBrokerService() {
		return brokerService;
	}

	public void setBrokerService(BrokerService brokerService) {
		this.brokerService = brokerService;
	}

}
