### Candidate Chinese Name:
* 
 徐家飞
- - -  
### Please write down some feedback about the question(could be in Chinese):
* 
* 分析：代码部分：
 * 	   SocketFinder是关于socket的一个工具类，无需重构
 * 	   JmsMessageBrokerSupport具备4个功能：创建MQ服务代理、停止MQ服务代理、发送消息、读取消息、监控消息队列是否处理完毕
 *     因为是消息模块的功能，是一个基础功能，依赖的多是基础组件，因此基础组件部分需要重构
 * 	   JmsMessageBrokerSupportTest用例包括消息发送、消息接收、未读取到消息的异常
 * 	         需求部分： 1.新API要满足所有应用程序中调用
 * 			  2.根据SOLID原则，切换不同MQ代理技术的时候，要灵活且低耦合
 *            3.按照类的SRP设计
 *            4.最好设计成流式API
 *            5.需要向下兼容
 *            6.如果需要可以采用依赖注入
 * 设计：重构JmsMessageBrokerSupport中依赖具体MQ部分：创建MQ服务、停止MQ服务、发送和读取消息的公共方法、监控消息队列是否处理完毕
 * 	   设计一个MQ服务接口：具备提供MQ代理服务（设置基本属性、启动、停止）、提供MQ连接和监控消息队列   不同的MQ代理技术单独实现该服务接口  详见设计图（PD）
 * 	   向下兼容有两种实现方式：1.新建类继承原类，新的应用程序使用新建的子类
 * 					 2.原类中保留原来的实现，在这基础上增加判断调用程序版本，新版本走新逻辑 （考虑尽量无需更改测试用例，采用这种方式）
 *   流式API有了解过，但是平时用的不多
- - -